import Vue from 'vue';

Vue.directive('comp-transform', {
  bind(el, binding, vnode) {
    let current = 0;
    el.addEventListener('dblclick', function(){
      let effect;
      let increment = binding.value || 90;

      if(binding.arg == 'scale') {
        effect = `scale(${increment})`;
      }
      else {
        if(binding.modifiers.reverse) {
          current -= increment;
        }
        else {
          current += increment;
        }

        effect = `rotate(${current}deg)`;
      }
      
      this.style.transform = effect;
      
      if(binding.modifiers.animate) this.style.transition = 'transform 0.4s';
    });
  }
});