export default class GroupsService {
  constructor(resource) {
    this._resource = resource('v1/grupos');
  }

  list() {
    return this._resource
      .query()
      .then(
        res => res.json(),
        err => {
          console.log(err);
          throw new Error('Não foi possível carregar os Grupos! Por favor, tente mais tarde.');
        }
      );
  }
}