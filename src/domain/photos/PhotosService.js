export default class PhotosService {
  constructor(resource) {
    this._resource = resource('v1/fotos{/id}');
  }

  list() {
    return this._resource
      .query()
      .then(
        res => res.json(),
        err => {
          console.log(err);
          throw new Error('Não foi possível carregar as Fotos! Por favor, tente mais tarde.');
        }
      );
  }

  delete(id) {
    return this._resource
      .delete({ id })
      .then(
        null,
        err => {
          console.log(err);
          throw new Error('Ocorreu um erro ao remover a Foto! Por favor, tente mais tarde.');
        }
      );
  }

  register(photo) {
    if(photo._id){
      return this._resource.update({ id: photo._id }, photo);
    }
    else {
      return this._resource.save(photo);
    }
  }

  search(id) {
    return this._resource
      .get({ id })
      .then(res => res.json());
  }
}