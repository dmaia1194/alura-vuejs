export default class Photos {
  constructor(url='', grupo='', titulo='', descricao='') {
    this.url = url;
    this.grupo = grupo;
    this.titulo = titulo;
    this.descricao = descricao;
  }
}