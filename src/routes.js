import Home from './components/home/Home.vue';
import Edit from './components/edit/Edit.vue';

export const routes = [
  {
    path: '',
    name: 'home',
    text: 'Home',
    component: Home,
    menu: true
  },
  {
    path: '/register',
    name: 'register',
    text: 'Cadastrar',
    component: Edit,
    menu: true
  },
  {
    path: '/edit/:id',
    name: 'edit',
    component: Edit,
    menu: false
  },
  {
    path: '*',
    component: Home,
    menu: false
  }
]